#!/bin/bash

# restart fcgi
# kill fcgi
# start fcgi

for pid in expr `ps aux | grep -e "python.*runfcgi" | awk '{print $2}'` 
do
  kill $pid
done

NEW_RELIC_CONFIG_FILE=newrelic.ini newrelic-admin run-program /home/ishioka/eking/manage.py runfcgi host=127.0.0.1 port=8080 outlog=/var/log/nginx/fcgi.log errlog=/var/log/nginx/fcgi_error.log
#/home/ishioka/eking/manage.py runfcgi host=127.0.0.1 port=8080 outlog=/var/log/nginx/fcgi.log errlog=/var/log/nginx/fcgi_error.log
