from django.conf.urls import patterns, include, url
from rss.views import LatestEntriesFeed

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^(?P<page>\d+)/$', 'eking_show.views.index'),
    url(r'^playcount/(?P<page>\d+)/$', 'eking_show.views.playcount'),
    url(r'^quality/(?P<page>\d+)/$', 'eking_show.views.quality'),
    url(r'^evaluation/(?P<good_or_bad>\w+)/(?P<video_id>\d+)$', 'eking_show.views.evaluation'),
    url(r'^tag/(?P<tag_group_id>\d+)/(?P<page>\d+)/$', 'eking_show.views.tag'),
    url(r'^site/(?P<site_name>\w+)/(?P<page>\d+)/$', 'eking_show.views.site'),
    url(r'^title/(?P<page>\d+)/$', 'eking_show.views.title', name='title'),
    url(r'^title/(?P<page>\d+)/(?P<search_string>\w+)/$', 'eking_show.views.title', name='title'),
    url(r'^video/(?P<video_id>\d+)$', 'eking_show.views.video'),
    url(r'^video_test/(?P<video_id>\d+)$', 'eking_show.views.video_test'),
    url(r'^link/$', 'eking_show.views.link'),
    url(r'^policy/$', 'eking_show.views.policy'),
    url(r'^cha/$', 'eking_show.views.cha'),
    url(r'^enc/$', 'eking_show.views.enc'),
    url(r'^int/$', 'eking_show.views.int'),
    url(r'^live/$', 'eking_show.views.live'),
    url(r'^rss/$', 'eking_show.views.rss'),
    url(r'^test/$', 'eking_show.views.test'),
    url(r'^latest/feed/$', LatestEntriesFeed()),
    # url(r'^eking/', include('eking.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'', 'eking_show.views.index'),
)
