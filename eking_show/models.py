from django.db import models
from django.db.models import Q
from operator import itemgetter, attrgetter
from datetime import datetime
import logging
import random


# Create your models here.
class Tag_Group(models.Model):
    group_name = models.CharField(max_length=50)
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.group_name

    @staticmethod
    def get_Tag_Group(self):
       tag_group_list = self.objects.all().order_by('id') 
       return tag_group_list

    @staticmethod
    def get_Tag_Name(self, tag_group_id):
       return self.objects.get(pk=tag_group_id)
 
class Video(models.Model):
    url = models.CharField(max_length=512)
    site = models.CharField(max_length=64)
    title = models.CharField(max_length=255)
    title_jpn = models.CharField(max_length=255, null=True, blank=True)
    video_code = models.CharField(max_length=2048)
    video_code_iphone = models.CharField(max_length=2048)
    thumb_path = models.CharField(max_length=1024)
    quality = models.IntegerField()
    play_num = models.IntegerField()
    minits = models.IntegerField()
    good_num = models.IntegerField()
    bad_num = models.IntegerField()
    tag_group = models.ForeignKey(Tag_Group, null=True, blank=True)
    tag_group2 = models.ForeignKey(Tag_Group, related_name='+', null=True, blank=True) 
    tag_group3 = models.ForeignKey(Tag_Group, related_name='+', null=True, blank=True) 
    show_flg = models.BooleanField(default=False)
    jpn_flg = models.BooleanField(default=False)
    pub_date = models.DateTimeField('date published')

    def __unicode__ (self):
        return self.video_code

    @staticmethod
    def get_japanese_videos(self, sort_column='pub_date'):
       #in_query = self.objects.filter(video_tag_relation__tag__name__startswith='japan').values('pk').distinct().query 
       #video_list = self.objects.filter(video_tag_relation__tag__name__startswith='japan')
       #now_time = dt.now()
       #string_time = 
       video_list = self.objects.filter(show_flg=True).filter(pub_date__lte=datetime.now())
       #video_list = list(set(video_list))
       logging.basicConfig(filename='/tmp/example.log',level=logging.DEBUG)

       if sort_column == "quality" :
           for video in video_list :
               video.quality = video.good_num - video.bad_num
           video_list = sorted(video_list, key=attrgetter(sort_column,'good_num','play_num'), reverse=True)
       else :
           video_list = sorted(video_list, key=attrgetter(sort_column), reverse=True)

       return video_list 

    @staticmethod
    def get_japanese_videos_by_tag(self, tag_group_id=0, tag_group2_id=0, tag_group3_id=0, own_video_id=0):
       #video_list = self.objects.filter(video_tag_relation__tag__tag_group_relation__tag_group=tag_group_id).filter(video_tag_relation__tag__name__startswith='japan') 

       if own_video_id != 0 :
        video_list = self.objects.filter( Q(show_flg=True), Q(tag_group=tag_group_id) | Q(tag_group2=tag_group_id) | Q(tag_group3=tag_group_id))\
        .filter( Q(tag_group=tag_group2_id) | Q(tag_group2=tag_group2_id) | Q(tag_group3=tag_group2_id))\
        .filter( Q(tag_group=tag_group3_id) | Q(tag_group3=tag_group2_id) | Q(tag_group3=tag_group3_id))\
        .filter( ~Q(id=own_video_id) )\
        .filter(pub_date__lte=datetime.now())

        video_list = list(video_list)

        if len(video_list) < 8:
            video_list2 = self.objects.filter( Q(show_flg=True), Q(tag_group=tag_group_id) | Q(tag_group2=tag_group_id) | Q(tag_group3=tag_group_id))\
            .filter( Q(tag_group=tag_group2_id) | Q(tag_group2=tag_group2_id) | Q(tag_group3=tag_group2_id))\
            .filter( ~Q(id=own_video_id) )\
            .filter(pub_date__lte=datetime.now())

            video_list = video_list + list(video_list2)
        
        if len(video_list) < 8:
            video_list3 = self.objects.filter( Q(show_flg=True), Q(tag_group=tag_group_id) | Q(tag_group2=tag_group_id) | Q(tag_group3=tag_group_id))\
            .filter( ~Q(id=own_video_id) )\
            .filter(pub_date__lte=datetime.now())

            video_list = video_list + list(video_list3)

        video_list = list(set(video_list))

        sampling_number = len(video_list) if len(video_list) < 8 else 8
        video_list = random.sample(video_list, sampling_number)

       else : 
        video_list = self.objects.filter( Q(show_flg=True), Q(tag_group=tag_group_id) | Q(tag_group2=tag_group_id) | Q(tag_group3=tag_group_id))\
        .filter( ~Q(id=own_video_id) )\
        .filter(pub_date__lte=datetime.now())

       video_list = sorted(video_list, key=attrgetter('pub_date'), reverse=True)

       return video_list 

    @staticmethod
    def get_japanese_videos_by_title(self, title):
       #video_list = self.objects.filter(video_tag_relation__tag__tag_group_relation__tag_group=tag_group_id).filter(video_tag_relation__tag__name__startswith='japan') 
       video_list = self.objects.filter(show_flg=True).filter(title_jpn__contains=title).filter(pub_date__lte=datetime.now())
       #video_list = list(set(video_list))
       video_list = sorted(video_list, key=attrgetter('pub_date'), reverse=True)

       return video_list 

    @staticmethod
    def get_japanese_videos_by_site(self, site="xvideos"):
       video_list = self.objects.filter(show_flg=True).filter(site__exact=site).filter(pub_date__lte=datetime.now())
       video_list = sorted(video_list, key=attrgetter('pub_date'), reverse=True)

       return video_list

    class Meta:
        unique_together = ("site", "title")

class VideoJpn(models.Model):
    url = models.CharField(max_length=500)
    title = models.CharField(max_length=255, unique=True)
    title_jpn = models.CharField(max_length=255)
    video_code = models.CharField(max_length=255)
    thumb_path = models.CharField(max_length=255)
    quality = models.IntegerField()
    play_num = models.IntegerField()
    minits = models.IntegerField()
    good_num = models.IntegerField()
    bad_num = models.IntegerField()
    show_flg = models.BooleanField(default=0)
    pub_date = models.DateTimeField('date published')

    def __unicode__ (self):
        return self.title

class Tag(models.Model):
    name = models.CharField(max_length=50, unique=True)
    pub_date = models.DateTimeField('date published')
  
    def __unicode__(self):
        return self.name

class Video_Tag_Relation(models.Model):
    video = models.ForeignKey(Video)
    tag = models.ForeignKey(Tag) 
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        video_name = Video.objects.get(pk=self.video.id).title
        tag_name = Tag.objects.get(pk=self.tag.id).name
        return "[ VIDEO NAME : " + video_name + " ] "+ "[TAG : " + tag_name + " ]"

   
class Tag_Group_Relation(models.Model):
    tag_group = models.ForeignKey(Tag_Group) 
    tag = models.ForeignKey(Tag) 
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        tag_group_name = Tag_Group.objects.get(pk=self.tag_group.id).group_name
        tag_name = Tag.objects.get(pk=self.tag.id).name
        return "[ TAG GROUP NAME : " + tag_group_name + " ] "+ "[TAG : " + tag_name + " ]"
