from eking_show.models import *
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import SimpleListFilter
from django.forms import TextInput, Textarea

class JpnVideoListFilter(SimpleListFilter):
    title = _('jpnVideo')
    parameter_name = 'jpnVideo'
    
    def lookups(self, request, model_admin):
        return(
            ('on', _('jpn videos only')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'on':
            return queryset.filter(jpn_flg=1)

class QualityVideoListFilter(SimpleListFilter):
    title = _('quality')
    parameter_name = 'quality'
    
    def lookups(self, request, model_admin):
        return(
            ('60', _('over 60')),
            ('70', _('over 70')),
            ('80', _('over 80')),
            ('90', _('over 90')),
        )

    def queryset(self, request, queryset):
        if self.value() == '60':
            return queryset.filter(quality__gte=60)
        if self.value() == '70':
            return queryset.filter(quality__gte=70)
        if self.value() == '80':
            return queryset.filter(quality__gte=80)
        if self.value() == '90':
            return queryset.filter(quality__gte=90)

class MinitsVideoListFilter(SimpleListFilter):
    title = _('minits')
    parameter_name = 'minits'
    
    def lookups(self, request, model_admin):
        return(
            ('5', _('over 5')),
            ('10', _('over 10')),
            ('15', _('over 15')),
            ('20', _('over 20')),
            ('30', _('over 30')),
            ('40', _('over 40')),
            ('50', _('over 50')),
        )

    def queryset(self, request, queryset):
        if self.value() == '5':
            return queryset.filter(minits__gte=5)
        if self.value() == '10':
            return queryset.filter(minits__gte=10)
        if self.value() == '15':
            return queryset.filter(minits__gte=15)
        if self.value() == '20':
            return queryset.filter(minits__gte=20)
        if self.value() == '30':
            return queryset.filter(minits__gte=30)
        if self.value() == '40':
            return queryset.filter(minits__gte=40)
        if self.value() == '50':
            return queryset.filter(minits__gte=50)

class SiteVideoListFilter(SimpleListFilter):
    title = _('site')
    parameter_name = 'site'
    
    def lookups(self, request, model_admin):
        return(
            ('pipii', _('site is pipii')),
            ('xvideos_jp', _('site is xvideos_jp')),
            ('ero_video', _('site is ero video')),
            ('tokyo_tube', _('site is tokyo tube')),
            ('asg', _('site is asg')),
            ('fc2', _('site is fc2')),
            ('xvideos', _('site is xvideos')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'pipii':
            return queryset.filter(site='pipii')
        if self.value() == 'ero_video':
            return queryset.filter(site='ero_video')
        if self.value() == 'tokyo_tube':
            return queryset.filter(site='tokyo_tube')
        if self.value() == 'asg':
            return queryset.filter(site='asg')
        if self.value() == 'fc2':
            return queryset.filter(site='fc2')
        if self.value() == 'xvideos':
            return queryset.filter(site='xvideos')
        if self.value() == 'xvideos_jp':
            return queryset.filter(site='xvideos_jp')


class PlayNumVideoListFilter(SimpleListFilter):
    title = _('playNum')
    parameter_name = 'playNum'
    
    def lookups(self, request, model_admin):
        return(
            ('50', _('over 50')),
            ('100', _('over 100')),
            ('200', _('over 200')),
            ('300', _('over 300')),
            ('400', _('over 400')),
            ('500', _('over 500')),
            ('1000', _('over 1000')),
            ('5000', _('over 5000')),
            ('10000', _('over 10000')),
        )

    def queryset(self, request, queryset):
        if self.value() == '50':
            return queryset.filter(play_num__gte=50)
        if self.value() == '100':
            return queryset.filter(play_num__gte=100)
        if self.value() == '200':
            return queryset.filter(play_num__gte=200)
        if self.value() == '300':
            return queryset.filter(play_num__gte=300)
        if self.value() == '400':
            return queryset.filter(play_num__gte=400)
        if self.value() == '500':
            return queryset.filter(play_num__gte=500)
        if self.value() == '1000':
            return queryset.filter(play_num__gte=1000)
        if self.value() == '5000':
            return queryset.filter(play_num__gte=5000)
        if self.value() == '10000':
            return queryset.filter(play_num__gte=10000)

class ShowFlgListFilter(SimpleListFilter):
    title = _('showflg')
    parameter_name = 'showflg'
    
    def lookups(self, request, model_admin):
        return(
            ('on', _('show flg on')),
            ('off', _('show flg off')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'on':
            return queryset.filter(show_flg=True)
        if self.value() == 'off':
            return queryset.filter(show_flg=False)

class VideoAdmin(admin.ModelAdmin):
    list_display = ('title','decade_thumb','minits','show_flg','pub_date')
    list_filter = (JpnVideoListFilter,QualityVideoListFilter,SiteVideoListFilter,PlayNumVideoListFilter,MinitsVideoListFilter, ShowFlgListFilter)
    #fields = ('video','title_jpn','tag_group','tag_group2','tag_group3','show_flg','pub_date')

    #fieldsets = (
    #    (None, {
    #        'fields': ('title','title_jpn','video_code'),
    #    }),
    #    (None, {
    #        'fields': ('url','quality','play_num','minits','show_flg','pub_date','decade_video_code'),
    #    }),
    #) 

    exclude = ('thumb_path','quality','play_num','good_num','bad_num')

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'250'})},
        models.TextField: {'widget': Textarea(attrs={'rows':4, 'cols':40})},
    } 

    readonly_fields = ('decade_video_code','video_code_iphone')

    def decade_thumb(self, obj):
        return "<img width=200 hight=148 src=\"" + obj.thumb_path + "\">"
    decade_thumb.allow_tags = True

    def decade_video_code(self, obj):
        return "<p>" + obj.video_code + "</p>"
    decade_video_code.allow_tags = True

    def video(self, instance):
        return instance.video_code
    video.allow_tags = True
    #change_form_template = 'change_form_video.html'

admin.site.register(Video,VideoAdmin)
admin.site.register(VideoJpn)
admin.site.register(Tag)
admin.site.register(Video_Tag_Relation)
admin.site.register(Tag_Group)
admin.site.register(Tag_Group_Relation)
