from django.http import HttpResponse
from django.http import HttpResponseNotFound 
from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from eking_show.models import Video
from eking_show.models import Tag_Group
from django.core.paginator import Paginator
from operator import itemgetter, attrgetter
from django.core.exceptions import ObjectDoesNotExist
from ua_parser import user_agent_parser
import logging
import json

VIDEO_NUM_PER_PAGE = 28

def index (request, page=1):
    try:
        video_list = Video.get_japanese_videos(Video)
        p = Paginator(video_list, VIDEO_NUM_PER_PAGE)
        tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)

        #logging.basicConfig(filename='/tmp/example.log',level=logging.DEBUG)
        #logging.debug(dir(video_list[0]))
        return render_to_response('eking/index.html',{\
            'latest_video_list' : p.page(page).object_list,\
            'pagination' : p, \
            'page' : p.page(page) ,\
            'search_type' : 'new', \
            'tag_group_list' : tag_group_list }) 
    except Exception as e:
        return HttpResponseNotFound("<h1>Video not found </h1>")

def site (request, site_name, page=1):
    try:
        video_list = Video.get_japanese_videos_by_site(Video, site_name)
        p = Paginator(video_list, VIDEO_NUM_PER_PAGE)
        tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)

        #logging.basicConfig(filename='/tmp/example.log',level=logging.DEBUG)
        #logging.debug(site_name)
        return render_to_response('eking/index.html',{\
            'latest_video_list' : p.page(page).object_list,\
            'pagination' : p, \
            'page' : p.page(page) ,\
            'search_type' : 'site', \
            'site_name' : site_name, \
            'tag_group_list' : tag_group_list }) 
    except Exception as e:
        return HttpResponseNotFound("<h1>Video not found </h1>")

def evaluation (request, good_or_bad, video_id):
    try :
        video = Video.objects.get(pk=video_id)

        if good_or_bad == "good" :
            video.good_num = video.good_num + 1
            video.save()
        elif good_or_bad == "hate" :
            video.bad_num = video.bad_num + 1
            video.save()

        response_data = {}
        response_data['result'] = 'update'

        #logging.basicConfig(filename='/tmp/example.log',level=logging.DEBUG)
        #logging.debug(json.dumps(response_data))

        return HttpResponse(json.dumps(response_data), content_type="application/json") 

    except ObjectDoesNotExist as e:
        return HttpResponseNotFound("<h1>Video not found </h1>")

def playcount (request, page=1):
    try:
        video_list = Video.get_japanese_videos(Video, 'play_num')
        p = Paginator(video_list, VIDEO_NUM_PER_PAGE)
        tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
        return render_to_response('eking/index.html',{'latest_video_list' : p.page(page).object_list, 'pagination' : p, 'page' : p.page(page) , 'search_type' : 'playcount', 'tag_group_list' : tag_group_list}) 
    except Exception as e:
        return HttpResponseNotFound("<h1>Video not found </h1>")

def quality (request, page=1):
   video_list = Video.get_japanese_videos(Video, 'quality')
   p = Paginator(video_list, VIDEO_NUM_PER_PAGE)
   tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
   return render_to_response('eking/index.html',{'latest_video_list' : p.page(page).object_list, 'pagination' : p,'page' : p.page(page) , 'search_type' : 'quality' , 'tag_group_list' : tag_group_list}) 

def tag (request, tag_group_id, page=1):
    try:
        video_list = Video.get_japanese_videos_by_tag(Video, tag_group_id)
        p = Paginator(video_list, VIDEO_NUM_PER_PAGE)
        tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
        tag_group_name = Tag_Group.get_Tag_Name(Tag_Group, tag_group_id)
        return render_to_response('eking/index.html', {'latest_video_list' : p.page(page).object_list, 'pagination' : p, 'page' : p.page(page), 'search_type' : tag_group_name, 'tag_group_id':tag_group_id , 'tag_group_list' : tag_group_list}) 
    except Exception as e:
        return HttpResponseNotFound("<h1>Video not found </h1>")

def title (request, page=1, search_string=None ):
    try:
        #logging.basicConfig(filename='/tmp/example.log',level=logging.DEBUG)
        #logging.debug(search_string)

        if search_string :
            title = search_string
        else :
            title = request.POST['url']


        video_list = Video.get_japanese_videos_by_title(Video, title)
        p = Paginator(video_list, VIDEO_NUM_PER_PAGE)
        tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
        return render_to_response('eking/index.html', {'latest_video_list' : p.page(page).object_list, 'pagination' : p, 'page' : p.page(page), 'search_type' : 'title', 'word':title , 'tag_group_list' : tag_group_list }) 
    except Exception as e:
        return HttpResponseNotFound("<h1>Title not found </h1>")

def video (request, video_id):
    try :
        video = Video.objects.get(pk=video_id)

        video.play_num = video.play_num + 1
        video.save()

        tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
        same_tag_video_list = Video.get_japanese_videos_by_tag(Video, video.tag_group_id, video.tag_group2_id, video.tag_group3_id, video_id)

        # On the server, you could use a WebOB request object.
        user_agent_string = request.META.get('HTTP_USER_AGENT')

        # Get back a big dictionary of all the goodies.
        result_dict = user_agent_parser.Parse(user_agent_string)
       
        #print result_dict['device']
        #logging.basicConfig(filename='/tmp/example.log',level=logging.DEBUG)
        #logging.debug(user_agent_string)
        device_name = result_dict['device']['family']
        #logging.debug(device_name)

        if device_name == "iPhone" or device_name == "iPad" or device_name == "iPod" :
            mobile_flag = 1
        else :
            mobile_flag = 0

        logging.debug(mobile_flag)

        return render_to_response('eking/video.html',{'video' : video, \
            'mobile_flag' : mobile_flag,\
            'tag_group_list' : tag_group_list,\
            'same_tag_video_list' : same_tag_video_list}) 

    except ObjectDoesNotExist as e:
        return HttpResponseNotFound("<h1>Video not found </h1>")

def video_test (request, video_id):

    video = Video.objects.get(pk=video_id)

    video.play_num = video.play_num + 1
    video.save()

    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    same_tag_video_list = Video.get_japanese_videos_by_tag(Video, video.tag_group_id, video_id)

    # On the server, you could use a WebOB request object.
    user_agent_string = request.META.get('HTTP_USER_AGENT')

    # Get back a big dictionary of all the goodies.
    result_dict = user_agent_parser.Parse(user_agent_string)
   
    #print result_dict['device']
    #logging.basicConfig(filename='/tmp/example.log',level=logging.DEBUG)
    #logging.debug(user_agent_string)
    device_name = result_dict['device']['family']
    #logging.debug(device_name)

    if device_name == "iPhone" or device_name == "iPad" or device_name == "iPod" :
        mobile_flag = 1
    else :
        mobile_flag = 0

    logging.debug(mobile_flag)

    return render_to_response('eking/video_test.html',{'video' : video, \
        'mobile_flag' : mobile_flag,\
        'tag_group_list' : tag_group_list,\
        'same_tag_video_list' : same_tag_video_list}) 


def link (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/link.html', { 'tag_group_list':tag_group_list}) 

def policy (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/policy.html',{ 'tag_group_list':tag_group_list} ) 

def ad (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/ad.html',{ 'tag_group_list':tag_group_list}) 

def cha (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/cha.html',{ 'tag_group_list':tag_group_list}) 

def enc (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/enc.html',{ 'tag_group_list':tag_group_list}) 

def int (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/int.html',{ 'tag_group_list':tag_group_list}) 

def live (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/live.html',{ 'tag_group_list':tag_group_list}) 

def rss (request):
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/rss.html',{ 'tag_group_list':tag_group_list}) 

def test (request, page=1):
    video_list = Video.get_japanese_videos(Video)
    p = Paginator(video_list, VIDEO_NUM_PER_PAGE)
    tag_group_list = Tag_Group.get_Tag_Group(Tag_Group)
    return render_to_response('eking/test.html',{'latest_video_list' : p.page(page).object_list, 'pagination' : p, 'page' : p.page(page) ,'search_type' : 'new', 'tag_group_list' : tag_group_list }) 
