# -*- coding: utf-8 -*-

import logging
import re
import requests 
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from eking_show.models import *
   
class Command(BaseCommand):
    args = 'no arguments need'
    help = u'xvideo 上でdeleteされているvideoのshow_flgを0にする'
            
    def handle(self, *args, **options):
        print("start video del check : " + datetime.now().isoformat())
        videos = Video.objects.filter(show_flg='1') 
        for video in videos:
            r = requests.get(video.thumb_path)
            if r.status_code == 404:
                #print(str(video.id))
                #print(str(video.title_jpn))

                video.show_flg = 0;
                video.save()
        print("finish video del chek")
