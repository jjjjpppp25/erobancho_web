# coding: utf-8
 
import logging
import re
from django.core.management.base import BaseCommand, CommandError
from eking_show.models import *
   
class Command(BaseCommand):
    args = 'no arguments need'
    help = u'videosテーブルのsite=fc2のthumb_pathをjpgに置き換える'
            
    def handle(self, *args, **options):
        videos = Video.objects.filter(site='fc2').filter(thumb_path__endswith='.gif') 
        for video in videos:
            video.thumb_path = video.thumb_path.replace('digest_','').replace('FCUT_','').replace('gif','jpg')
            video.save()
            #print(video.thumb_path)
        #img_path = 'http://video34.fc2.com/up/thumb/201302/24/N/digest_20130224NmrGuh9U.gif'
        #print(img_path.replace('digest_','').replace('gif','jpg'))
