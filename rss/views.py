# -*- coding: utf-8 -*-

from django.contrib.syndication.views import Feed
from eking_show.models import *

class LatestEntriesFeed(Feed):
    title = "Erobancho.com New"
    link = "/new/"
    description = "Update Videos to Erobancho.com"

    def items(self):
        return Video.objects.filter(show_flg=True).filter(pub_date__lte=datetime.now).order_by('-pub_date')[:10]

    def item_title(self, item):
        return item.title_jpn

    def item_link(self, item):
        return "http://www.erobancho.com/video/" + str(item.id)

    def item_description(self, item):
        return "再生時間:" + str(item.minits) + " 再生回数:" + str(item.play_num) 
